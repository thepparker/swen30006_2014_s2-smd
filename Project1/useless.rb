class HTML
    def initialize(doctype = "html", tabstop=4)
        @doctype = doctype
        @tabstop = tabstop

        @indent = 0

        # html_list is a list of lists, with each item containing and indent
        # and the corresponding data
        @html_list = []

        _add_item("<!DOCTYPE #{doctype}>", 0)
    end

    def to_s
        # dumps the html array into a string
        str = ""

        @html_list.each do | indent, item |
            str += _indent_to_space(indent) + item
        end

        return str
    end
    
    def open_tag tag
        # public wrapper for _add_item
        _add_item(tag, @indent)
        @indent += 1
    end

    def close_tag tag
        @indent -= 1
        _add_item(tag, @indent)

        if @indent < 0
            @indent = 0
        end
    end

    def add_data data
        # Adds a chunk of data to the html list. Uses the current
        # @indent
        _add_item(data, @indent)
    end

    
    def _add_item(data, indent)
        # Adds a tag to the html list with the given indent. Can be used to
        # bypass the @indent increment for special tags
        @html_list << [ indent, data + "\n" ]
    end

    
    def _indent_to_space indent
        # Converts an indent to the given tabstop. Default is 
        # 1 indent == 4 spaces
        return " " * @tabstop * indent
    end
end
