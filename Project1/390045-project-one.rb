#!/usr/bin/env ruby

##
# 390045-project-one.rb Requires Ruby 1.9.3+
#
# A simple application to decipher a Caesar Cipher, without knowing the shift
# beforehand. Also provides simple methods to encode and decode a text string 
# with a known shift amount.
# 
# The guessing algorithm is documented in the methods utilising it.
#
# For HTML outputs, the css style '1' will give a simple page with the encoded
# and decoded text side by side. Style '2' will give the encoded and decoded
# text side by side, but with a scrollbar that is locked to each view, such
# that if one column is scrolled, the other will scroll too. Lastly, style '3' 
# will show only the decrypted text filling the entire page. These styles are
# not 3 *completely distinct* styles, but in reality there are very few ways to
# neatly display long sections of text, short of tabulating. Style 2 utilises 
# jQuery, so the included jquery-2.1.1.min.js must be in the current directory.
#
# Prithu Parker 390045
#

class Caesar
    def initialize(shift = 0, alphabet = ("a".."z").to_a)
        @shift = shift

        # Leave alphabet as an array for easy creation of the shift string
        @alphabet = alphabet

        # Construct our standard a-zA-Z alphabet here, because it will never
        # change.
        @def_alphabet = alphabet.join
        @def_alphabet = @def_alphabet + @def_alphabet.upcase

        # Create weightings for `decode_with_guess`
        # Weightings taken from https://en.wikipedia.org/wiki/Letter_frequency
        @weighting = {
            "a" => 0.0817, "b" => 0.0149, "c" => 0.0278, "d" => 0.0425, 
            "e" => 0.1270, "f" => 0.0223, "g" => 0.0202, "h" => 0.0609, 
            "i" => 0.0697, "j" => 0.0015, "k" => 0.0077, "l" => 0.0403, 
            "m" => 0.0241, "n" => 0.0675, "o" => 0.0751, "p" => 0.0193, 
            "q" => 0.0010, "r" => 0.0599, "s" => 0.0633, "t" => 0.0906, 
            "u" => 0.0276, "v" => 0.0098, "w" => 0.0236, "x" => 0.0015, 
            "y" => 0.0197, "z" => 0.0007
        }
    end

    def encode(str, shift=@shift)
        # Use Caesar cipher to encrypt the given string with the set shift
        # amount.
        #
        # We _could_ do this the long way, and iterate over an array, replacing
        # each char with char+26, but Ruby has a neat string method called 'tr'
        # which lets us replace characters in a string with the corresponding
        # characters in a second string. For example, if we have a string of
        # "HELLO", and we wanted to replace the letters 'L' and 'O' with
        # something else, we can use "HELLO".tr('LO', 'YA') to replace each
        # occurrence of 'L' with 'Y' and each occurrence of 'O' with 'A',
        # resulting in a string of "HEYYA". 
        # To this end, if we establish a string of characters 'a-zA-Z', 
        # along with a shifted string of characters, we can use 'tr' to
        # directly replace occurrences of letters with the shifted ones.
        # EG: We can directly translate characters using these two strings -
        # abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ (0 shift)
        # qrstuvwxyzabcdefghijklmnopQRSTUVWXYZABCDEFGHIJKLMNOP (16 shift)
        # Meaning, q = a, b = r, c = s, etc. in the encrypted text.

        # Use modulo shift because shifts > 26 are just wrapping around the
        # alphabet again. i.e. shift of 27 == shift of 1.
        shift = shift % @alphabet.size
        
        # now we want a string rotated by shift for replacing the plain chars
        shifted = @alphabet.rotate(shift).join
        shifted = shifted + shifted.upcase

        # simply use tr to encode our str!
        str.tr(@def_alphabet, shifted)
    end

    def decode(str, shift=@shift)
        # Decrypt the given str based on the given shift. This is literally the
        # reverse of encode.
        shift = shift % @alphabet.size

        shifted = @alphabet.rotate(shift).join
        shifted = shifted + shifted.upcase

        # Just use tr in the opposite direction!
        str.tr(shifted, @def_alphabet)
    end

    def decode_with_guess str
        # Attempts to guess the shift amount of a string encoded with Caesar
        # cipher. To do so, we use a frequency based analysis and weightings
        # from Wikipedia (https://en.wikipedia.org/wiki/Letter_frequency). 
        # Initial attempt was to solve based on the algorithm written in PHP
        # by Robert Eisele, available here: 
        # http://www.xarg.org/2010/05/cracking-a-caesar-cipher/
        #
        # However, that algorithm proved to only work for strings that had the
        # letter 'E' in them. This implementation is based on the Caesar 
        # cracking method by Rob Spectre, available on GitHub at 
        # https://github.com/RobSpectre/Caesar-Cipher
        #
        # We calculate every potential string that could be obtained by
        # reverse shifting (25 different strings), and then calculate an
        # entropy for each string based on a letter frequency analysis. The
        # smaller the entropy, the more likely that it is our shift.
        
        # First we generate a hash of weightings => deciphered text
        weights = Hash.new
        decoded = Hash.new

        (1..25).to_a.each do |i|
            deciphered = self.decode(str, i)
            weight = self.string_entropy deciphered

            weights[i] = weight
            decoded[i] = deciphered
        end

        # Now, whichever key has the smallest value in weights corresponds to 
        # the shift our message has. We can just get the key out directly
        # from the sorted Hash
        _shift = weights.sort_by { |k, v| v }
        
        # _shift is a list in the format [[s, w], [s, w] ...]
        shift = _shift[0][0]
        return [shift, decoded[shift]]
    end

    def string_entropy str
        # We convert all chars to lowercase and get the alpha characters so we
        # can get the weightings from @weighting by key.
        # The result of this is the weighting of each character multipled
        # and having the log taken of the result. 
        # i.e log2(@w'a') + log2(@w'b') = log2(@w'a'*@w'b')
        # This gives us an entropy value. If we take the entropy of proper
        # English strings, we can see that the (absolute) value is smaller
        # (because the frequency is larger for common letters) when the log2 is
        # taken. i.e abs(log2(E)) < abs(log2(Z)). From this, we can see that if
        # we were to take the log2 of all possibilities, whichever is
        # smallest has the most commonly used English letters, and hence is
        # the most likely correct string. Works in all cases I have tested,
        # including those with strange structures (like vowels missing).
        total = 0
        str.downcase.scan(/[[:alpha:]]/).each do |c|
            total += Math.log2(@weighting[c]).abs
        end

        total
    end
end

class PrettyOut
    def initialize(type, style=nil)
        @type = type
        @style = style
    end

    def print_result(encoded, shift, decoded)
        case @type
        when "html"
            print_html(encoded, shift, decoded)
        when "text"
            print_text(encoded, shift, decoded)
        end
    end

    private
    def print_html(encoded, shift, decoded)
        # When printing html, we replace \r\n (or \n or \r) in the 
        # encoded/decoded strings with <br> so they render nicely in HTML
        encoded = encoded.gsub(/\r?\n|\r/, "<br>")
        decoded = decoded.gsub(/\r?\n|\r/, "<br>")

        File.open('template.html', "r+") do |f|
            h = f.read()
            h = h.gsub(/\$ENCODED\$/, encoded).gsub(/\$DECODED\$/, decoded)
            h = h.gsub(/\$STYLENUM\$/, @style).gsub(/\$SHIFT\$/, shift.to_s)

            puts h
        end
    end

    private
    def print_text(encoded, shift, decoded)
        puts "The decrypted text is:"
        puts decoded
        puts "Which was shifted by " + shift.to_s + " letters."
    end
end

def main
    input = ARGV[0]
    output_type = ARGV[1]
    css_style = ARGV[2]

    if input == nil or output_type == nil
        puts "Usage: #{$0} \"input file\" \"output type\" <style num>"
        exit
    end

    caesar = Caesar.new
    output = PrettyOut.new(output_type, css_style)

    str_to_decode = IO.read input

    shift, decoded = caesar.decode_with_guess(str_to_decode)

    output.print_result(str_to_decode, shift, decoded)
end

main


