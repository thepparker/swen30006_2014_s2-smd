class ApplicationController < ActionController::Base
  layout "main"
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user

  def authenticate
    redirect_to login_path unless !current_user.nil?
  end

  private
  def current_user
    @current_user ||= User.find(session[:uid]) if session[:uid]
  end

  private
  def set_session_user user
    session[:uid] = user.id
  end
end
