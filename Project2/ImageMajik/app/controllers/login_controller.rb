class LoginController < ApplicationController
  def new
    # just serves a login form
  end

  def create
    # Support login via username or email. Both are unique.
    user = User.verify_user(params[:login], params[:password])

    if user
      set_session_user user
      redirect_to root_url, :notice => "Successfully logged in"
    else
      flash.now.alert = "Invalid username or password"
      render "new"
    end
  end

  def destroy
    session[:uid] = nil

    redirect_to root_url, :notice => "Logged out"
  end
end
