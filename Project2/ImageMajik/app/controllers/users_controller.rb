# users_controller.rb
# Very basic user authentication/registration based on
# http://railscasts.com/episodes/250-authentication-from-scratch

class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end
  def new
    # Temp user, not saved. Lets us validate user registration and serve
    # registration errors
    @user = User.new
  end

  def create
    @user = User.create(user_params)

    if @user.save
      set_session_user @user
      redirect_to root_url, :notice => "Successfully registered"
    else
      render "new"
    end
  end

  def destroy
  end

  def info
    # Returns a json dict with the usernames of all registered users. This is
    # required for sharing images.

    users = {"all" => []}
    @user = current_user

    if @user == nil
      render nothing: true, status: :unauthorized and return
    end

    User.find_each do |user|
      if user != @user
        users["all"] << [ user.username, user.id ]
      end
    end

    render json: users
  end

  private
  def user_params
    params.require(:user).permit(:name, :username, :email, :password, 
      :password_confirmation)
  end
end
