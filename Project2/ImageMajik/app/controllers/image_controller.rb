class ImageController < ApplicationController
  helper_method :image_popover_id

  before_action :authenticate

  def view
    @user = current_user

    @owned_images = []
    @shared_images = []
    @trashed_images = []

    @user.image_shares.each do |imshare|
      if imshare.versions.any? { |ver| ver.trash == false }
        @owned_images << imshare.image
      end

      if imshare.versions.any? { |ver| ver.trash == true }
        @trashed_images << imshare.image
      end
    end

    @user.shared_image_shares.each do |imshare|
      @shared_images << imshare.image
    end

    # Separate array for setting up popovers. This will create a popover for 
    # every image the user can see
    @user_images = @owned_images | @trashed_images
  end

  def edit
    image_id = params[:image_id].to_i

    # image_data is an image data URL, in the form of 
    # data:<mime>;base64,<encoded data>
    image_data = params[:filtered_image]

    if (image_id == "" or image_id == nil or image_data == "" or 
      image_data == nil)

      redirect_to :back, :notice => "Invalid image edit request" and return
    end

    # We should check the mime type first so ensure it's something we support
    data_arr = image_data.split(';')
    # Use some simple string comprehension to get the image data, since the
    # format is guaranteed
    data = data_arr[1][7..-1]

    if data_arr.length != 2 or data == "" or data == nil
      redirect_to :back, :notice => "Malformed image data" and return
    end

    mime = data_arr[0].split(':')[1]
    if not validate_file(mime)
      redirect_to :back, :notice => "Invalid image mime type" and return
    end

    @user = current_user

    # Check user permissions
    imshare = @user.image_shares.to_a.find { |ims|
      ims.image.id == image_id
    }
    # no image share owned by the user has a corresponding image id. therefore
    # unauthorized
    if imshare == nil
      render nothing: true, status: :unauthorized and return
    end

    image = imshare.image

    # Now we need to convert the data in data_arr to binary for saving.
    bindata = Base64.decode64(data)

    file_name = hash_image_name image.image_name
    write_image_file(file_name, bindata)

    image_location = "#{IMAGE_RELATIVE_PATH}/#{file_name}"
    image.versions.create!(image_location: image_location,
                           file_name: file_name)
    image.save

    redirect_to :back, :notice => "Filter applied and new version created"
  end

  def upload
    # Image data is contained in the ':image' parameter
    uploaded = params[:image]

    if uploaded == "" or uploaded == nil
      redirect_to :back, :notice => "No image specified" and return
    end

    mime = Rack::Mime.mime_type(File.extname(uploaded.original_filename))
    if validate_file(mime)
      # We generate a name based on the current epoch, md5 hash of the 
      # filename, and a random number.
      file_name = hash_image_name uploaded.original_filename

      # Write the image data to file
      write_image_file(file_name, uploaded.read)

      image_name = uploaded.original_filename
      image_location = "#{IMAGE_RELATIVE_PATH}/#{file_name}"

      # Create the image associations
      imshare = current_user.image_shares.create
      imshare.owner = current_user

      image = Image.create!(image_name: uploaded.original_filename)
      imshare.image = image
      # use bang create, so if it fails there's an exception and nothing is
      # saved
      imshare.image.versions.create!(image_location: image_location,
                                     file_name: file_name)
      image.save

      flash[:notice] = 'Image successfully uploaded'
      redirect_to :back

    else
      redirect_to :back, :notice => "Invalid file" and return
    end
  end

  def restore
    # ImageVersion gets passed to this method. We need to check if the
    # image belongs to the user
    imv_id = params[:id].to_i
    @user = current_user

    imv = @user.image_versions.find(imv_id)
    if imv == nil
      render nothing: true, status: :not_found and return
    end

    imshare = @user.image_shares.to_a.find { |ims|
      ims.versions.any? { |ver| ver == imv }
    }
    # no image share with matching ver found that belongs to the user
    if imshare == nil
      render nothing: true, status: :unauthorized and return
    end

    # Got the image version and it belongs to use. If it's marked as trash,
    # unmark it.
    if imv.trash
      imv.trash = false

      imv.save

      redirect_to :back, :notice => "Image has been restored"
    end
  end
	
	def delete
    # ImageVersion gets passed to this method. We need to check if the
    # image belongs to the user
    imv_id = params[:id].to_i
    @user = current_user

    imv = @user.image_versions.find(imv_id)
    if imv == nil
      render nothing: true, status: :not_found and return
    end

    imshare = @user.image_shares.to_a.find { |ims|
      ims.versions.any? { |ver| ver == imv }
    }
    # no image share with matching ver found that belongs to the user
    if imshare == nil
      render nothing: true, status: :unauthorized and return
    end

    # Got the image version. If the version is not marked as trash, mark
    # it as trash. If it is already trashed, destroy the version.
    if !imv.trash
      imv.trash = true

      imv.save

      redirect_to :back, :notice => "Image version moved to trash"
    else
      # Delete the file from the OS, then destroy the appropriate records
      delete_image_file(imv.file_name)

      image = imshare.image

      # If this is the last version, destroy the share and image too!
      if image.versions.length == 1
        imshare.destroy
        image.destroy
      end
      
      imv.destroy

      redirect_to :back, :notice => "Image version permanently deleted"
    end
  end

  def share
    image_id = params[:image_id]
    share_users = params[:share_users]

    puts share_users.to_yaml

    if image_id == nil or image_id == ""
      render nothing: true, status: :not_found and return
    elsif share_users == nil or share_users == ""
      redirect_to :back, :notice => "No users selected to share with" and return
    end

    image = Image.find(params[:image_id])

    @user = current_user
    # Check permissions, as usual
    imshare = @user.image_shares.to_a.find { |ims|
      ims.image == image
    }

    # see delete/edit
    if imshare == nil
      render nothing: true, status: :unauthorized and return
    end

    # clear the shared_users, so we just set it to the users which have been
    # specified. this allows us to 'remove' shared users by simply not 
    # selecting them when sharing
    imshare.shared_users.clear

    # get users based on the users list
    users = User.find(share_users)

    users.each do |user|
      imshare.shared_users << user
      user.save
    end

    imshare.save

    redirect_to :back, :notice => "Image has been shared" and return
  end

  def get_image_info
    image = Image.find(params[:id])
    version_id = params[:vid].to_i
    version = image.versions.to_a.find { |ver| 
      ver.id == version_id
    }

    if image == nil or version == nil
      render nothing: true, status: :not_found and return
    end

    first_version = image.versions.first
    latest_version = image.versions.last

    data = {
      "image_id" => image.id,
      "image_name" => image.image_name,

      "latest_version" => {
        "id" => latest_version.id,
        "location" => latest_version.image_location
      },

      "first_version" => {
        "id" => first_version.id,
        "location" => first_version.image_location
      },

      "specified_version" => {
        "id" => version.id,
        "location" => version.image_location
      }
    }

    render json: data
  end

  rescue_from ActiveRecord::RecordInvalid do
    redirect_to :back, :notice => 'Error creating record', :success => false, 
                       :status => :unprocessable_entity
  end

  private
  def validate_file(mime = nil)
    return IMAGE_MIME_TYPES.include?(mime)
  end

  private
  def write_image_file(file_name, data)
    # Create upload directory if it doesn't exist
    FileUtils.mkdir_p(IMAGE_SAVE_PATH)
    File.open("#{IMAGE_SAVE_PATH}/#{file_name}", 'wb') do |f|

      f.write(data)
    end
  end

  private
  def delete_image_file(file_name)
    file = "#{IMAGE_SAVE_PATH}/#{file_name}"
    if File.exists?(file)
      File.delete(file)
    end
  end

  private
  def image_popover_id(image, version)
    "popover_#{image.id}_#{version.id}"
  end

  private
  def hash_image_name(filename)
    md5_hash = Digest::MD5.hexdigest filename
    epoch = Time.now.to_i.to_s
    randhex = SecureRandom::hex 8
    filetype = File.extname filename

    return "#{epoch}_#{md5_hash}_#{randhex}#{filetype}"
  end
end
