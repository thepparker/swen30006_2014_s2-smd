class ImageShare < ActiveRecord::Base
  # ImageShare associations. Belongs to a user, has many images/versions
  # Allow user access to ImageShare

  # owner user
  belongs_to :owner, :class_name => "User"

  # Setup references to shared users
  has_and_belongs_to_many :shared_users, :class_name => "User"

  # Setup references to images
  has_one :image
  has_many :versions, :through => :image, :class_name => "ImageVersion"

  validates_presence_of :owner
  validates_presence_of :image
end
