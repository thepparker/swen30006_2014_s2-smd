class User < ActiveRecord::Base
  # Attributes:
  # t.string :name
  # t.string :username
  # t.string :hashed_password
  # t.string :email

  # Setup associations. We could use point to point, 
  # i.e. user->imageshares->image, but rails has 'through' assocation, which
  # eases our work significantly.

  attr_accessor :password

  # Setup image associations. ANY image share selected through image_shares
  # will be owned by the user. Any image shares in shared_image_shares are
  # shares that do NOT belong to the user.
  has_many :image_shares, :foreign_key => 'owner_id'
  has_many :images, :through => :image_shares
  has_many :image_versions, :through => :images, :source => :versions
  has_and_belongs_to_many :shared_image_shares, :class_name => "ImageShare"

  validates_confirmation_of :password
  validates_presence_of :name
  validates_presence_of :username
  validates_presence_of :email
  validates_presence_of :password, :on => :create

  validates_uniqueness_of :username, case_sensitive: false
  validates_uniqueness_of :email, case_sensitive: false
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  before_save :hash_password
  before_save { self.email = email.downcase }

  def self.verify_user(login, password)
    user = find_by(username: login) || find_by(email: login)

    if !user
      return nil
    end

    pass_to_hash = user.password_salt + password
    hashed = OpenSSL::Digest::SHA256.hexdigest pass_to_hash

    if user.hashed_password == hashed
      # hashed pws match. user authenticated!
      return user
    else
      return nil
    end
  end

  def hash_password
    # This is called before the entity is flushed to the database. It's here
    # where we salt and hash the password. Passwords are not stored in
    # plaintext.
    if password.present?
      self.password_salt = SecureRandom::hex(32)
      pass_to_hash = self.password_salt + password

      self.hashed_password = OpenSSL::Digest::SHA256.hexdigest pass_to_hash
    end
  end

end
