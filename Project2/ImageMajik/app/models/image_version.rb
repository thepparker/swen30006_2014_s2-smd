class ImageVersion < ActiveRecord::Base
  # Attributes:
  # t.string :image_location
  # t.string :file_name
  # t.boolean :trash

  validates_presence_of :image_location
  validates_presence_of :file_name

  after_initialize :set_defaults

  belongs_to :image

  def set_defaults
    self.trash = false if self.trash.nil?
  end
end
