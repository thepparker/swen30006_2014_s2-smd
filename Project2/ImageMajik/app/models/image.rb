class Image < ActiveRecord::Base
  # Attributes:
  # t.string :image_name

  # Setup ImageShare link
  belongs_to :image_share

  # Setup versions
  has_many :versions, :class_name => "ImageVersion"

  validates_presence_of :image_name
end
