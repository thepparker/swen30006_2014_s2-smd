Rails.application.routes.draw do
  get 'login' => 'login#new'
  get 'logout' => 'login#destroy'
  get 'register' => 'users#new'

  get 'image/info/:id/:vid' => 'image#get_image_info'
  get 'image/view'
  post 'image/share'
  post 'image/upload'
  post 'image/edit'
  post 'image_version.:id' => 'image#restore'
  delete 'image_version.:id' => 'image#delete'

  get 'users/info'

  root 'general#index'

  resources :users
  resources :login
  resource :image_version
end
