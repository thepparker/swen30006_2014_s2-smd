# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

# Set upload paths
IMAGE_SAVE_PATH = Rails.root.join('public', 'uploads')
IMAGE_RELATIVE_PATH = '/uploads'

# Set accepted image mime types
IMAGE_MIME_TYPES = ["image/png", "image/jpeg", "image/gif", "image/tiff"]
