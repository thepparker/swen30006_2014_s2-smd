class RemoveImageNameFromImageVersions < ActiveRecord::Migration
  def change
    remove_column :image_versions, :image_name, :string
  end
end
