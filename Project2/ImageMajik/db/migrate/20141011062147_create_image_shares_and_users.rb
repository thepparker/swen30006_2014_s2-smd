class CreateImageSharesAndUsers < ActiveRecord::Migration
  # This is a table to associate image shares with users. i.e. a table join/
  def change
    create_table :image_shares_users, id: false do |t|
      t.references :user
      t.references :image_share
    end
  end
end
