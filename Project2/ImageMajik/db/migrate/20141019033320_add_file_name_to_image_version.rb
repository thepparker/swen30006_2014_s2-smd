class AddFileNameToImageVersion < ActiveRecord::Migration
  def change
    add_column :image_versions, :file_name, :string
  end
end
