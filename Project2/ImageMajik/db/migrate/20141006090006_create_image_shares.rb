class CreateImageShares < ActiveRecord::Migration
  def change
    create_table :image_shares do |t|
      t.references :owner

      t.timestamps
    end
  end
end
