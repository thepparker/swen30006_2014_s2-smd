class CreateImageVersions < ActiveRecord::Migration
  def change
    create_table :image_versions do |t|
      t.string :image_location
      t.string :image_name
      t.integer :filter_type

      t.boolean :trash

      t.belongs_to :image

      t.timestamps
    end
  end
end
