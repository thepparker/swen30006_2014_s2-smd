class RemoveFilterTypeFromImageVersion < ActiveRecord::Migration
  def change
    remove_column :image_versions, :filter_type, :string
  end
end
